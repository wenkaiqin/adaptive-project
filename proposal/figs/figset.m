function [] = figset(fs, fn)
% figset : Set current axes with input font size and font name.  Also remove
%          title.
%
%
% INPUTS
%
% fs ---------- font size
%
% fn ---------- font name (e.g., 'times', 'monospaced', 'freemono').  See
%               listfonts(gca) for a list of all possible fonts.  Note that
%               some fonts are rendered identically to others.
%
%+==============================================================================+%

set(gca, 'fontsize', fs, 'fontname', fn);
hy = get(gca, 'ylabel');
hx = get(gca, 'xlabel');
set(hy, 'fontsize', fs, 'fontname', fn);
set(hx, 'fontsize', fs, 'fontname', fn);
title('');
