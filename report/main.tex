\documentclass[12pt]{article} % Set the font size to 12pt and use the 'article' class
\usepackage[utf8]{inputenc} % Set the input encoding to UTF-8
\usepackage[margin=1in]{geometry} % Set the margins to 1 inch on all sides
\usepackage{setspace} % Load the 'setspace' package to control line spacing
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}
\singlespacing % Set single-spacing for the text

\newcommand{\Rthree}{\ensuremath{\mathbb{R}^{3}}}
\newcommand{\Rthth}{\ensuremath{\mathbb{R}^{3 \times 3}}}
\newcommand{\Rfour}{\ensuremath{\mathbb{R}^{4}}}
\newcommand{\Rsix}{\ensuremath{\mathbb{R}^{6}}}
\newcommand{\Rtwelve}{\ensuremath{\mathbb{R}^{12}}}
\newcommand{\Rone}{\ensuremath{\mathbb{R}}}
\newcommand{\SOth}{\ensuremath{\mathsf{SO}(3)}}
\newcommand{\soth}{\ensuremath{\mathfrak{so}(3)}}
\newcommand{\Lone}{\ensuremath{\mathcal{L}_1}~}
\newcommand{\T}{\ensuremath{^{\mathsf{T}}}}
\newcommand{\Bperp}{\ensuremath{B^{\perp}}}
\newcommand{\Bbar}{\ensuremath{\bar{B}}}
\newcommand{\sa}{\ensuremath{\sigma_a}}
\newcommand{\sha}{\ensuremath{\hat{\sigma}_a}}
\newcommand{\sua}{\ensuremath{\sigma_{ua}}}
\newcommand{\sh}{\ensuremath{\hat{\sigma}}}
\newcommand{\s}{\ensuremath{\sigma}}
\newcommand{\shua}{\ensuremath{\hat{\sigma}_{ua}}}
\newcommand{\urej}{\ensuremath{u_{rej}}}
\newcommand{\ztilde}{\ensuremath{\tilde{z}}}
\newcommand{\zhat}{\ensuremath{\hat{z}}}

% For tagging align* environments.
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

% Redefine the maketitle command to reduce space above the title
\makeatletter
\renewcommand{\maketitle}{%
  \begin{center}
    \vspace*{-1.5cm} % Adjust the space above the title
    {\LARGE \@title}\\
    \vspace{1em}
    {\large \@author}\\
    \vspace{1em}
    {\small \@date}\\
  \end{center}
}
\makeatother


\title{System ID and Adaptive Control Project Report: \\ ~\vspace{-0.2em} \\ \Lone Adaptive Augmentation for Geometric Tracking Control of Quadrotors}
\author{Wenkai Qin}
\date{April 29, 2024}

\begin{document}

\maketitle

\section{Introduction}
\label{sec:intro}

Unmanned aerial vehicles have recently seen a dramatic rise in use across many
sectors, both public and private. Their applications include search and
rescue, cinematography, agriculture, public safety, and warfare. The quadrotor
platform, in particular, has exploded in popularity due to its agility,
portability, and simplicity. However, as the system is intrisincally
underactuated and nonlinear, the platform has also proven to be a challenging
system to control, especially in the face of disturbances such as wind, ground
effect, etc. When confronted with sudden and severe uncertainties in the
system, conventional control methods like decoupled PD linear systems fail,
sometimes causing accidents, loss of property, or worse.

To this end, researchers have applied many control theoretic approaches to the
problem of quadcopter control under extrinsic or unpredictable disturbances.
While assumptions-based methods such as taking linear approximations and
focusing on certain classes of disturbances are possible, their limited scope
results in conservative applicability. Linear models in particular tend to
fail at significant departures from the linearization points, meaning that
aggressive trajectories are unable to be tracked. Several adaptive control
studies remain cognizant of the nonlinear dynamics, but make constant
disturbance approximations, rendering them inoperable under time-varying or
state-dependent disturbances. Sliding-mode control approaches have yielded
chattering actuator problems due to their switching nature. While these
chattering issues were relieved in a follow-on study, the remedy relied on
Euler angle formulations, which are well-known to possess troublesome
singularities.

Another area of active research has harnessed machine learning tools such as
deep neural networks and Gaussian process regression. These methods require no
a priori knowledge and can learn disturbance functions accurately based on
only input-output training data. However, these methods fail to react to
unmodelled factors such as sloshing or system degradation (underpowered motor,
chipped propeller, etc.) and fail when these changes depart significantly from
the training data.

Wu et al. present a method in \cite{wu2022L1quadrotors} that builds upon the
nonlinear geometric tracker proposed by Lee et al. in \cite{lee2010geometric}. They
choose the \Lone adaptive control scheme to compensate for nonlinear,
time-varying, and state-dependent uncertainties for three primary reasons:
\begin{enumerate}
    \item Generally, \Lone adaptive control can predict and reject uncertainties at an arbitrarily fast rate, limited only by the hardware capabilities.
    \item The fast adaptation is separated from control action via a low pass filter (LPF), thus separating and protecting the overall closed-loop system from high-frequency effects and instability.
    \item \Lone adaptive control has already been experimentally validated on a number of flying vehicles, including NASA's AirStar and Calspan's Learjet.
\end{enumerate}
\noindent The authors of \cite{wu2022L1quadrotors} also aim to improve upon other
iterations of \Lone adaptive control by:
\begin{enumerate}
    \item Applying the \Lone adaptive augmentation to the both translational and rotational dynamics, in constrast to other authors who had focused solely on the rotational dynamics, and
    \item Using a piecewise-constant adaptation law as opposed to a projection or gradient-based optimization, schemes that can introduce numerical issues under large adaptation gains.
\end{enumerate}
The proposed adaptive controller relies on the nonlinear geometric controller
from \cite{lee2010geometric} to ensure exponential trajectory tracking
stability under nominal conditions (i.e., no disturbances), and uses its \Lone
adaptive augmentation to compensate for external disturbances.
\cite{wu2022L1quadrotors} continues by implementing and validating the
adaptive controller on a physical Mambo quadrotor, pictured in
\cite[Fig.~1]{wu2022L1quadrotors}.

\section{Dynamics}

This section reiterates the dynamics of a quadrotor, adopting the nomenclature
from \cite{lee2010geometric}.
\begin{align*}
    m \in \Rone &: \text{total mass} \\
    J \in \Rthth &: \text{inertia tensor} \\
    p \in \Rthree &: \text{position in the inertial frame} \\
    v \in \Rthree &: \text{velocity in the inertial frame} \\
    \Omega \in \Rthree &: \text{body angular rate in body frame} \\
    R \in \SOth &: \text{inertial to body frame rotation matrix} \\
    M \in \Rone &: \text{total moment in the body frame} \\
    f \in \Rone &: \text{total thrust}
\end{align*}
The equations of motion (EOM) are thus
\begin{align}
    \dot{p}      &= v                                   \label{eq:eom1} \\
    m\dot{v}     &= mge_{3} - fRe_{3}                   \label{eq:eom2} \\
    \dot{R}      &= R \hat{\Omega}                      \label{eq:eom3} \\
    \dot{\Omega} &=J^{-1} (M - \Omega \times J \Omega)  \label{eq:eom4}
\end{align}
where the hat operator $\hat{\cdot}: \Rthree \rightarrow
 \soth$ used in (\ref{eq:eom3}) represents the cross-product equivalent matrix
such that $x \times y = \hat{x}y$ where $x,y \in \Rthree$ and $\hat{x} \in
\Rthth$. Here, (\ref{eq:eom1}) and (\ref{eq:eom2}) describe the second-order
translational body dynamics, while (\ref{eq:eom3}) and (\ref{eq:eom4})
describe the second-order rotational body dynamics.

We briefly reiterate the nonlinear geometric controller from
\cite{lee2010geometric} while deliberating avoiding delving into the details.
However, we note that the following control laws result in exponential
stability for the quadrotor reference trajectory problem under nominal
conditions. In (\ref{eq:m_control}), $K_R$, $K_\Omega \in \Rthth$ are
user-chosen positive definite proportional and derivative gain matrices,
respectively operating on the proportional and derivative rotational errors
$e_R$ and $e_\Omega$. $R_d$ and $\Omega_d$ represent the desired inertial to
body matrix and body rates. The same pattern applies to translational control
variables $K_p$, $K_v$, $e_p$, $e_v$, and $x_d$ in (\ref{eq:f_control}).

\begin{align}
    M &= -K_R e_R - K_\Omega e_\Omega + \Omega \times J\Omega - J(\hat{\Omega}R\T R_d \Omega_d - R\T R_d \dot{\Omega}_d)      \label{eq:m_control} \\
    f &= \frac{1}{e_3 \cdot Re_3}(-K_p e_p -K_v e_v -mge_3 + m \ddot{p}_d)                                                    \label{eq:f_control}
\end{align}

\section{\Lone Adaptive Augmentation}
\label{sec:l1ac}

This section first includes disturbances into the EOMs presented in Section
\ref{sec:intro}. Then, it develops the \Lone adaptive control augmentation
used.

First, define the state variable $x \in \Rtwelve$ as $x\T = \begin{bmatrix}p\T
& v\T & \phi & \theta & \psi & \Omega\T \end{bmatrix}$, where $\phi$,
$\theta$, and $\psi$ are the quadrotor Euler angles. We then take the partial
state $z \in \Rsix$ as $z\T = \begin{bmatrix} v\T & \Omega\T \end{bmatrix}$
and reform the second order translation and rotation dynamics (\ref{eq:eom2},
\ref{eq:eom4}) as a nonlinear state-space model on $z$. Note that in this
section, $e_1$, $e_2$, and $e_3$ refer to the standard basis vectors in
\Rthree~ (e.g., $e_3 = \begin{bmatrix} 0 & 0 & 1\end{bmatrix}\T$).

\begin{equation*}
    \dot{z}(t) = f(z(t)) + B(R(t)) u_b(t),
\end{equation*}
where $f(z) \in \Rsix$ and $B(R) \in \mathbb{R}^{6 \times 4}$ are defined as
\begin{align*}
    f(z) &= \begin{bmatrix} ge_3 \\ -J^{-1} \Omega \times J \Omega \end{bmatrix} \\
    B(R) &= \begin{bmatrix} m^{-1} R e_3 & 0_{3 \times 3} \\ 0_{3 \times 1} & J^{-1} \end{bmatrix}
\end{align*}
and $u_b \in \Rfour$ is defined as the baseline control input,
$u_b\T = \begin{bmatrix}  v\T & \Omega\T \end{bmatrix}$, given by the
nonlinear geometric controller. Now, suppose that uncertainties are introduced
to the system via (\ref{eq:eom2}, \ref{eq:eom4}). This can be written as

\begin{equation*}
    \dot{z}(t) = f(z(t)) + B(R(t)) u_b(t) + \begin{bmatrix} m^{-1} F_0 \\ J^{-1} M_0 \end{bmatrix},
\end{equation*}
where $F_0, M_0 \in \Rthree$ are extrinsic disturbances not
accounted for by the base EOMs. $F_0$ can be further split into "aligned" and
"unaligned components", where the aligned component refers to alignment with
the control input, i.e., aligned with thrust on the body z-axis. As the
baseline controller gives controllability on all three rotation axes, all
three elements of $M_0$ can be considered aligned. After this alignment
decomposition, the aligned components can then be directly grouped with the
control input in a single term, where the unaligned components must then be
introduced separately with an accompanying "secondary" control matrix.
\begin{align*}
    \dot{z}(t) &= f(z(t)) + B(R(t)) u_b(t) + \begin{bmatrix} m^{-1}       F_{ix} \\ m^{-1}       F_{iy} \\ m^{-1}       F_{iz} \\ J^{-1} M_0 \end{bmatrix} \\
               &= f(z(t)) + B(R(t)) u_b(t) + \begin{bmatrix} m^{-1} R e_1 F_{bx} \\ m^{-1} R e_2 F_{by} \\ m^{-1} R e_3 F_{bz} \\ J^{-1} M_0 \end{bmatrix} \\
               &= f(z(t)) + B(R(t)) u_b(t) + \begin{bmatrix} m^{-1} R e_3 & 0_{3 \times 3} \\ 0_{3 \times 1} & J^{-1}       \end{bmatrix} \begin{bmatrix} F_{bz} \\ M_0    \end{bmatrix}
                          +                  \begin{bmatrix} m^{-1} R e_1 & m^{-1} R e_2 \\ 0_{3 \times 1} & 0_{3 \times 1} \end{bmatrix}
                                             \begin{bmatrix} F_{bx} \\ F_{by} \end{bmatrix}
\end{align*}
We note that the control matrix $B(R)$ makes a reappearance in the second term, and we define the secondary control matrix
\begin{equation*}
    \Bperp(R) =  \begin{bmatrix} m^{-1} R e_1 & m^{-1} R e_2 \\ 0_{3 \times 1} & 0_{3 \times 1} \end{bmatrix} \in \mathbb{R}^{6 \times 2}
\end{equation*}
The aligned and unaligned external disturbance vectors shall henceforth be
referred to as $\sa$ and $\sua$, respectively. We also introduce disturbance
rejection control input $\urej$ to be used additively with the baseline
control input $u_b$. Thus, we have the disturbance-inclusive partial-state
dynamics as
\begin{equation}
    \dot{z}(t) = f(z(t)) + B(R(t)) \big( u_b(t) + \urej(t) + \sa(t,x(t)) \big) + \Bperp(R(t)) \sua (t,x(t)) \label{eq:z_dynamics}
\end{equation}

Now, we construct a state predictor and analyze the error dynamics.
\begin{equation}
    \dot{\zhat}(t) = f(z(t)) + B(R(t)) \big( u_b(t) + \urej(t) + \hat{\sa}(t,x(t)) \big) + \Bperp(R(t)) \shua (t,x(t)) + A_m \ztilde(t), \label{eq:z_predictor}
\end{equation}
where, in general, the signal error is defined as $\tilde{(\cdot)} =
\hat{(\cdot)} - (\cdot)$ and $A_m$ is a user-chosen Hurwitz matrix that will
stabilize the dynamics of $\ztilde$.

Subtracting (\ref{eq:z_predictor}) from (\ref{eq:z_dynamics}), we can then
arrive at the partial state error dynamics
\begin{multline*}
    \dot{\ztilde}(t) = \Big[ \cancel{f(z(t))} + B(R(t)) \big( \cancel{u_b(t)} + \cancel{\urej(t)} + \sha(t,x(t)) \big) + \Bperp(R(t)) {\shua} (t,x(t)) + A_m \ztilde(t) \Big] \\
    - \Big[ \cancel{f(z(t))} + B(R(t)) \big( \cancel{u_b(t)} + \cancel{\urej(t)} + \sa(t,x(t)) \big) + \Bperp(R(t)) \sua (t,x(t)) \Big]
\end{multline*}
\begin{equation*}
     = B(R(t)) \big(\sha(t,x(t)) - \sa(t,x(t)) \big) + \Bperp(R(t)) \big( \shua (t,x(t)) - \sua (t,x(t)) \big) + A_m \ztilde(t)
\end{equation*}
\begin{align}
                        &= A_m \ztilde(t) + \begin{bmatrix} B(R(t)) & \Bperp(R(t)) \end{bmatrix}
                            \left( \begin{bmatrix} \sha(t,x(t)) \\ \shua(t,x(t)) \end{bmatrix} -
                                   \begin{bmatrix} \sa (t,x(t)) \\ \sua (t,x(t)) \end{bmatrix} \right) \notag \\
    \dot{\ztilde}(t)  &= A_m \ztilde(t) + \Bbar(t) (\sh(t) - \s(t)) \label{eq:z_errdynamics}
\end{align}
Note the concatenations $\Bbar(R(t)) \triangleq \begin{bmatrix} B(R(t)) &
\Bperp(R(t)) \end{bmatrix}$ and $\s\T \triangleq \begin{bmatrix} \sa\T &
\sua\T \end{bmatrix}$. Also, $\Bbar$ remains invertible by construction as its
columns are mutually linearly dependent thanks to the $R e_i$ factors in the
first row terms of $\Bbar$.

In general, the solution to the linear system $\dot{x}(t) = A(t) x(t) + B(t) u(t)$ is
\begin{equation*}
    x(t) = \Phi_A(t,t_0) x(t_0) + \int_{t_0}^{t} \Phi_A(t,s)B(s)u(s)ds,
\end{equation*}
where $\Phi_A(t,t_0)$ is the state transition matrix from $t_0$ to $t$. For
time-invariant dynamics such that $A(t)=A$, $\Phi_A(t,t_0) = e^{A(t-t_0)}$.
Applying our own dynamics and input, we substitute $A(t) \leftarrow A_m$,
$B(t) \leftarrow \Bbar(t)$, and $u(t) \leftarrow \sh(t) - \s(t)$.
\begin{align}
    \ztilde(t) &= e^{A_m(t-t_0)} \ztilde(t_0) + \int_{t_0}^{t} e^{A_m(t-s)}\Bbar(s)(\sh(s) - \s(s))ds \nonumber \\
               &= e^{A_m(t-t_0)} \ztilde(t_0) + \int_{t_0}^{t} e^{A_m(t-s)}\Bbar(s) \sh(s) ds - \int_{t_0}^{t} e^{A_m(t-s)} \Bbar(s) \s(s)ds \label{eq:zt_inter}
\end{align}

Suppose we with to take the above solution for a small timestep past 0, such
that $t_0=0$ and $t=T_s$. Further, without loss of generality, assume the
initial partial state prediction error $\ztilde(0)$ is some nonzero value. We
aim to show that for a certain formulation of $\sh(t)$, $\ztilde(0)$ has no
effect on the value of $\ztilde(T_s)$. We begin by proposing a zero-order hold
(ZOH) assumption on $\sh(t)$ and $\Bbar(t)$ (i.e., take $\sh(t) = \sh(0)$ and
$\Bbar(t) = \Bbar(0)$ for $t \in [0,T_s]$) and expanding the second term in
(\ref{eq:zt_inter}). This assumption is valid for $\sh(t)$, as it is within
the power of the designer to alter the behavior of $\sh(t)$ at all times. In
contrast, it is not necessarily valid for $\Bbar(t)$, as it is not within the
power of the designer to hold $R$ constant for any period of time. The authors
of \cite{wu2022L1quadrotors} do not directly address this issue; however, the
assumption should nonetheless hold when $T_s$ remains small and the quadrotor
does not experience high body rates.
\begin{align*}
    \ztilde(T_s) &= e^{A_m T_s} \ztilde(0) + \int_{0}^{T_s} e^{A_m(T_s-s)}\Bbar(0) \sh ds - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= e^{A_m T_s} \ztilde(0) - \left(e^{A_m(T_s-s)}A_m^{-1}\biggr\rvert_{s=0}^{T_s} \right) \Bbar(0) \sh - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= e^{A_m T_s} \ztilde(0) - \left(e^{A_m(T_s-T_s)}A_m^{-1} - e^{A_m(T_s-0)}A_m^{-1} \right) \Bbar(0) \sh - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= e^{A_m T_s} \ztilde(0) - \left(e^0 - e^{A_m T_s} \right) A_m^{-1} \Bbar(0) \sh - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= e^{A_m T_s} \ztilde(0) + \left(e^{A_m T_s} - I \right) A_m^{-1} \Bbar(0) \sh - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
\end{align*}
Now, set $\sh$ such that the first two terms cancel.
\begin{equation}
    \sh \triangleq -\Bbar^{-1}(0) A_m \left(e^{A_m T_s} - I \right)^{-1} e^{A_m T_s} \ztilde(0) \label{eq:adap_law}
\end{equation}
\begin{equation*}
\begin{split}
    \ztilde(T_s) &= e^{A_m T_s} \ztilde(0) - \cancel{\left(e^{A_m T_s}  - I \right) A_m^{-1} \Bbar(0)}
                                              \cancel{\Bbar^{-1}(0) A_m \left(e^{A_m T_s} - I \right)^{-1}}
                                                        e^{A_m T_s} \ztilde(0) \\
                 &\hspace{22em} - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= \cancel{e^{A_m T_s} \ztilde(0)} - \cancel{e^{A_m T_s} \ztilde(0)} - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
                 &= -\cancel{e^{A_m T_s} \ztilde(0)} - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds \\
    \ztilde(T_s) &=  - \int_{0}^{T_s} e^{A_m(T_s-s)} \Bbar(0) \s(s)ds
\end{split}
\end{equation*}
Thus, we have shown that with the given assumptions, the initial error
$\ztilde(0)$ does not show up in the expression for the error after timestep
$z(T_s)$. Reapplying a ZOH to the timestep $t \in [T_s, 2T_s]$, we
can see that $z(T_s)$ will be eliminated in the expression for $z(2T_s)$, and
so on so forth. With proper tuning of $T_s$ and $A_m$, one may maintain a
small $||\ztilde||$.

To then reject aligned disturbances $\sa$, one could then simply set $\urej =
-\sa$. However, small values of $T_s$, which are required to maintain a small
$||\ztilde||$, may cause high adaptation gains via the $\left(e^{A_m T_s} - I
\right)^{-1}$ factor in (\ref{eq:adap_law}). This could cause chattering in
the actuator, excitement of unmodelled high-frequency behavior, and
deterioration of actuator components. To protect the overall system from these
effects, we apply a LPF with transfer function $C(s)$ to $\sa$, which is
standard for \Lone adaptive control schemes. With a slight abuse of notation,
\begin{equation*}
    \urej(s) = -C(s) \sha(s)
\end{equation*}

As such, we have derived the state predictor, adaptation law, and error
dynamics for the quadrotor \Lone adaptive augmentation. The partial state was
used such that \Bbar would be a square matrix; otherwise, the adaptation law
would be invalid. Fig. \ref{fig:block_diagram} shows how the various
components of the general control system fit with one another.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{figs/block_diagram.png}
  \caption{Block diagram of the \Lone adaptive control augmentation over a baseline controller. The \Lone augmentation is shown in blue.}
  \label{fig:block_diagram}
\end{figure}

\section{Implementation}
To demonstrate the proposed controller, a full system simulation was built in
MATLAB. The major components of the simulator are as follows. Note that this
section departs from the original paper in that the implementation was done
independently, as no code provided by the authors of \cite{wu2022L1quadrotors}
was used.
\begin{enumerate}
    \item Differential equations of motion.
    \begin{description}
        \item First, the equations of motion as described in Equations
        (\ref{eq:eom1}-\ref{eq:eom4}) were implemented in {\tt
        quadOdeFunction()}. This function takes the current state at a
        particular time (position, velocity, etc.) and a set of parameters
        (mass, moment, motor model, etc.) and outputs the rate of change of
        the state at the same particular time. {\tt quadOdeFunction()}
        primarily serves as a handle for MATLAB's {\tt ode45()}, which handles
        the actual propagation based on these equations of motion.
    \end{description}
    \item Simulation framework.
    \begin{description}
        \item The {\tt quadOdeFunction()} was then encapsulated with an {\tt
        ode45()} call in a simulator function called {\tt
        simulateQuadrotorDynamics()}. For a given timespan (usually 0.01 sec),
        this {\tt ode45()} call propagates {\tt quadOdeFunction()} from a
        starting state {\tt X0} and calculates a time history of the state
        {\tt X(t)} across the given timespan. This was then wrapped around
        with a {\tt for} loop across time. Using this setup, both ZOH software
        and hardware processes and fully continuous physics simulation could
        be modelled. The loop repeats this propagation/reset process until the
        full simulation timespan is completed, and ultimately results in a
        full trajectory.
    \end{description}
    \item Nonlinear controller.
    \begin{description}
        \item A 100 Hz nonlinear controller was built according to
        \cite{lee2010geometric} on a ZOH basis. Given the measured
        input and reference trajectory value at the beginning of each 0.01 s
        timestep, the required forces and moments $u_b$ would be calculated
        for that timestep. A simulated electronic speed controller would then
        calculate and set the required propeller speeds accordingly. An
        unscented Kalman filter with toggle functionality was built to
        simulate the measurements, if desired. The UKF used global navigation
        satellite system measurements and inertial measurement unit data to
        provide position, velocity, body rates, and orientation estimates.
    \end{description}
    \item Top level script.
    \begin{description}
        \item A top level script {\tt topSimulate.m} was created that provides
        {\tt simulateQuadrotor}-{\tt Dynamics()} with: (1) model descriptors
        for the quadcopter, (2) an initial state, and (3) a set of command and
        disturbance inputs. This script also processes and plots the outcoming
        data in a series of plots describing time histories, 3D trajectories,
        etc. Generally, the user will create their own version of this top
        level script for their own simulation particulars. For example, the
        next portion of this section describes the implementation of a top
        level script that contains the proper setup and commands to fly a
        quadrotor in a flat circle.
    \end{description}
\end{enumerate}

At this point, the \Lone adaptive controller was not yet implemented.
Extensive testing was done at this point to confirm that under nominal
conditions, with results presented in \ref{sec:results}. The initial condition
provided to the simulation was off-track to confirm stability of the
closed-loop system despite flight deviations from the reference trajectory.

After confirmation of proper trajectory following under nominal conditions
(i.e., no disturbances introduced), the state predictor, adaptatopm law, and
LPF were implemented. Due to the continuous nature of the state predictor,
some of the implementation was not implemented on the same ZOH basis as the
rest of the controller in this iteration of the software. This is not quite
realistic, as all software runs in discrete-time, and serves as a point for
future improvement.

The adaptive controller implementation was tested by reconstructing the same
figure-8 reference trajectory used in \cite{wu2022L1quadrotors}, injecting
time-varying disturbances into the simulated system, and observing the quality
of the trajectory following. First, the \Lone adaptive controller was turned
off to confirm that the presence of disturbances results in significant
departures from the reference trajectory. Then, \Lone adaptive control was
turned on to examine whether or not trajectory tracking performance was
returned. For testing purposes, similar time-varying disturbances were used as
found in \cite{wu2022L1quadrotors}. These disturbances were sinusoidal in nature and turned on at $t=5$ s.


\section{Results}
\label{sec:results}

This section presents results from the analysis conducted as decribed in the
previous section. All figures can be found following the references.

First, we present the baseline controller performance under no disturbances
and a figure-8 reference trajectory. As shown in Fig. \ref{fig:baseline_traj},
the baseline controller successfully guides the simulated quadrotor into the
desired trajectory with no issue. Fig. \ref{fig:baseline_tracking}, no
tracking was accomplished as there were no disturbances, and serves as a
comparison for following plots.

After injecting time-varying disturbances with \Lone adaptive control
\emph{off}, we observe that while the quadrotor initially followed the same
path as the no-disturbance test case, it also quickly became irrecoverably
unstable as soon as disturbances were injected as shown in Fig.
\ref{fig:fail_traj}. Fig. \ref{fig:fail_tracking} also clearly shows the lack
of disturbance tracking.

Finally, turning on \Lone adaptive control resulted in recovered performance.
While no longer following the prescribed figure-8 to the same level of
accuracy, basic flight is positively recovered, as shown in Fig.
\ref{fig:success_traj}. As shown in Fig. \ref{fig:success_tracking}, the
injected disturbances are followed and rejected with seeming ease. Upon close
examination, the effect of the LPF can also be seen as there is a slight delay
between the disturbance and rejection actuation.

The full source code can be found at
\url{https://gitlab.com/wenkaiqin/quadsim_matlab} under the {\tt
adaptive\_control} branch. Demonstration GIFs for all three test cases can be
found in the accompanying presentation.

\section{Conclusion}
This report presents the \Lone adaptive control augmentation of a nonlinear
geometric controller as shown by \cite{wu2022L1quadrotors}. It reimplements
the source code and proves via successive tests that the controller recovers
tracking performance lost due to injected disturbances.


\bibliographystyle{unsrt}
\bibliography{refs}


\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/baseline_traj.png}
  \caption{3D trajectory of the nonlinear controller under no injected disturbances. Nominal reference trajectory tracking is achieved.}
  \label{fig:baseline_traj}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/baseline_tracking.png}
  \caption{Injected disturbance and tracking capability of the baseline geometric controller. No disturbances were introduced, and no tracking was accomplished.}
  \label{fig:baseline_tracking}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/fail_traj.png}
  \caption{3D trajectory of the nonlinear controller with injected disturbances and \Lone adaptive control turned off. The quadrotor falls out of the sky.}
  \label{fig:fail_traj}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/fail_tracking.png}
  \caption{Injected disturbance and tracking capability with \Lone adaptive control off. Clearly, the disturbances were not tracked.}
  \label{fig:fail_tracking}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/success_traj.png}
  \caption{3D trajectory of the nonlinear controller with injected disturbances and \Lone adaptive control turned on. The quadrotor recovers tracking performance.}
  \label{fig:success_traj}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/success_tracking.png}
  \caption{Injected disturbance and tracking capability of the successful geometric controller. The disturbances were accurately estimated and rejected.}
  \label{fig:success_tracking}
\end{figure}

\end{document}
